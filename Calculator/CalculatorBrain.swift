//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by Uchenna Chilaka on 9/13/16.
//  Copyright © 2016 Uchenna Chilaka. All rights reserved.
//

import Foundation
/*
// global function - no longer needed
func multiply(op1: Double, op2: Double) -> Double {
    return op1 * op2
}
*/

class CalculatorBrain {
    
    private var accumulator = 0.0
    
    func setOperand(operand: Double) {
        accumulator = operand
    }
    
    private var operations : Dictionary<String, Operation> = [
        "π": Operation.Constant(M_PI),
        "e": Operation.Constant(M_E),
        "√": Operation.UnaryOperation(sqrt),
        "±": Operation.UnaryOperation({ -$0 }),
        "cos": Operation.UnaryOperation(cos),
        "×": Operation.BinaryOperation({ (opt1, opt2) -> Double in return opt1 * opt2 }), // expanded form of closures
        "÷": Operation.BinaryOperation({ $0 / $1 }), // concise version of closures
        "+": Operation.BinaryOperation({ $0 + $1 }),
        "-": Operation.BinaryOperation({ $0 - $1 }),
        "=": Operation.Equals
    ]
    
    // discreet values implementation of Swift
    // enums cannot have traditional variables
    // enums cannot have inheritance
    // enums are `pass by value` data structures
    // enums can have funcs
    // @FYI classes and enums can be nested -> creates effect of namespaces
    // @IMPORTANT all types should be capitalized
    // @IMPORTANT all lower case vars should be `camel cased`
    // enums have associated values
    enum Operation {
        case Constant(Double) // associated value for the Constant key in the Operation enum
        case UnaryOperation((Double) -> Double) // function that takes a double and returns a Double
        case BinaryOperation((Double, Double) -> Double)
        case Equals
    }
    
    func performOperation(symbol: String) {
        // this `if` block with variable assignment will effectively ignore any operations that
        // are not implemented in the Dictionary
        if let operation = operations[symbol] {
            switch operation {
                // fancy construct for associating values with constants and extracting
            // them in the case utilizing a local variable declaration inline
            case .Constant(let associatedConstantValue):
                accumulator = associatedConstantValue
                
            case .UnaryOperation(let associatedFunction):
                accumulator = associatedFunction(accumulator)
                
            case .BinaryOperation(let associatedFunction):
                executePendingBinaryOperation()
                pending = PendingBinaryOperationInfo(binaryFunction: associatedFunction, firstOperand: accumulator)
                
            case .Equals:
                executePendingBinaryOperation()
            }
        }
    }
    
    private func executePendingBinaryOperation() {
        if pending != nil {
            accumulator = pending!.binaryFunction(pending!.firstOperand, accumulator)
            // after operation, clear pending
            pending = nil
        }
        // no default needed if you address ALL of the possible values for `operation`
    }
    
    // creating an optional struct
    private var pending: PendingBinaryOperationInfo?
    
    // almost identical to class
    // can have stored AND computed vars
    // @IMPORTANT no inheritance!
    // struct, like enums, are passed by value -> Classes are passed by reference (lives in
    // the heap, and a pointer is passed to anyone with whom it is shared). `Passed by value`
    // means that when something is shared or passed to a method, it is `copied` to the reciever
    struct PendingBinaryOperationInfo {
        var binaryFunction: ((Double, Double) -> Double)
        var firstOperand: Double
    }
    
    var result : Double {
        // NOT implementing the `set` property of the computed variable
        // ensures that it is 'Read Only'
        get {
            return accumulator
        }
    }
    
}