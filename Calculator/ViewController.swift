//
//  ViewController.swift
//  Calculator
//
//  Created by Uchenna Chilaka on 9/7/16.
//  Copyright © 2016 Uchenna Chilaka. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // ! and ? both mean optional when declaring
    // Optional is always auto-initialized as "NOT SET"
    // ! in declaration will implicitly unwrap the Optional
    @IBOutlet private weak var display: UILabel!
    
    private var userIsInTheMiddleOfTyping = false
    
    @IBAction private func touchDigit(sender: UIButton) {
        let digit = sender.currentTitle!
        // display is an Optional in declaration, so MUST be unwrapped with "!"
        // You need to do the same for the text message you're sending to display
        if userIsInTheMiddleOfTyping {
            let textCurrentlyInDisplay = display.text!
            // you can SET an optional and update it's associated value
            // Optional = nil will return the state of an Optional to NOT SET
            display.text = textCurrentlyInDisplay + digit
        } else {
            display.text = digit
        }
        userIsInTheMiddleOfTyping = true
    }
    
    // computed property -> awesome! for tracking things and computing 
    private var displayValue : Double {
        get {
            // if you ever put a string in here, it WILL crash - this computed 
            // variable is designed to ONLY accept Double values
            return Double(display.text!)!
        }
        set {
            // unlike double, there's no optional challenge with 
            // Strings - it will take ANY value
            // `newValue` -> is a special keyword that will represent whatever 
            // someone set to this computed variable
            display.text = String(newValue)
        }
    }
    
    // @IMPORTANT - here, we are not using `brain : CalculatorBrain because Swift can 
    // infer the type based on the initializer assignment
    private var brain = CalculatorBrain()
    
    @IBAction private func performOperation(sender: UIButton) {
        if userIsInTheMiddleOfTyping {
            brain.setOperand(displayValue)
            userIsInTheMiddleOfTyping = false
        }
        // gracefully unwrap ONLY if the sender's currentTitle is set
        if let mathematicalSymbol = sender.currentTitle {
            /* 
             // @BAD -> started here for lesson, but moving all the login into the CalculatorBrain
             // which is the `Model` in this MVC
            if mathematicalSymbol == "π" {
                /*
                // create new instances of object String
                // M_PI input is a double, utilizing String initializer from double
                display.text = String(M_PI)
                */
                // Old way above; After adding `displayValue` computed variable, now ->
                displayValue = M_PI
            } else if mathematicalSymbol == "√" {
                displayValue = sqrt(displayValue)
            }
            */
            brain.performOperation(mathematicalSymbol)
        }
        // set result after brain operation is complete
        displayValue = brain.result
    }
}

